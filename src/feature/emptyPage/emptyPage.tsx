import { useRouteError } from "react-router-dom";
import { Image } from "antd";

export default function EmptyPage() {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection:'column'
      }}
    >
      <h1>Oops!</h1>
      <p>Page is empty.</p>
      <Image
        width={500}
        src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
      />
    </div>
  );
}
