import {
  DownOutlined,
  LeftOutlined,
  RightOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Button, Dropdown, MenuProps, Space, message } from "antd";
import Table from "antd/es/table";
import { TableRowSelection } from "antd/es/table/interface";
import React, {
  forwardRef,
  useImperativeHandle,
  useState,
} from "react";
import { columns } from "../../utils/buttonData";
import { DataType } from "../../utils/type";
import "./TableData.scss";

const TableData = ({ data, setvisiable, setDataSearch }: any, ref: React.Ref<unknown> | undefined) => {
  const [padination, setPagination] = useState<string>("10/Trang");
  const [page, setPage] = useState<number>(0);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  //pass selectedRowKeys children to parent
  useImperativeHandle(ref, () => ({
    someExposedProperty: () => {
      return selectedRowKeys;
    },
  }));

  const onSelectChange = (newSelectedRowKeys: React.Key[], selectedRows: any) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: "odd",
        text: "Select Odd Row",
        onSelect: (changeableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
      {
        key: "even",
        text: "Select Even Row",
        onSelect: (changeableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
    ],
  };

  const items: MenuProps["items"] = [
    {
      label: "20/Trang",
      key: "1",
      icon: <UserOutlined />,
    },
    {
      label: "30/Trang",
      key: "2",
      icon: <UserOutlined />,
    },
    {
      label: "40/Trang",
      key: "3",
      icon: <UserOutlined />,
    },
    {
      label: "50/Trang",
      key: "4",
      icon: <UserOutlined />,
    },
  ];

  const handleMenuClick: MenuProps["onClick"] = (e) => {
    const response: any = items.filter(
      (itemData: any) => itemData.key === e.key
    );
    setPagination(response[0].label);
  };

  const menuProps = {
    items,
    onClick: handleMenuClick,
  };

  const upPage = () => {
    setPage(page + 1);
  };

  const downPage = () => {
    setPage(page - 1);
  };

  return (
    <div className="tableDataContainer">
      <div className="topContent">
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          pagination={false}
          style={{ width: "100%", height: "100%", overflow: "hidden" }}
          // scroll={{ x: 950, y: "calc(100vh - 220px)" }} //fixed when load mutilple data
          onRow={(record, rowIndex) => {
            return {
              onClick: (event) => {
                console.log('event', record)
                setvisiable(true) //open modal
                setDataSearch(record) // get value record so that Show record when pop update show
              }, 
            };
          }}

        />
      </div>
      <div className="bottomContent">
        <div className="leftBottom">
          <p className="titleItem">Total: {data.length} item</p>
        </div>
        <div className="rightBottom">
          <div className="blockPaging">
            <LeftOutlined onClick={downPage} />
            <div className="page">{page}</div>
            <RightOutlined onClick={upPage} />
          </div>
          <Dropdown menu={menuProps}>
            <Button>
              <Space>
                {padination}
                <DownOutlined />
              </Space>
            </Button>
          </Dropdown>
        </div>
      </div>
    </div>
  );
};

export default forwardRef(TableData);
