import "./dashboard.scss";
import { buttonAction, dataTable } from "../utils/buttonData";
import TableData from "./component/TableData";
import { useEffect, useRef, useState } from "react";
import { Modal, notification, Select } from "antd";
import { Button, Form, Input } from "antd";
import {
  DownOutlined,
  ExclamationCircleOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { layout, validateMessages } from "../utils/form";
import _ from "lodash";
import { TypeDataSearch } from "../utils/type";

const DastBoardScreen = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [open, setOpen] = useState(false);
  const [dataSelected, setDataSelected] = useState<string>("");
  const buttonRef = useRef<any>(null);
  const [nameProduct, setNameProduct] = useState("");
  const [email, setEmail] = useState("");
  const [openSearch, setOpenSearch] = useState<boolean>(false);
  const [dataList, setDataList] = useState<TypeDataSearch[]>(dataTable);
  const [visiable, setVisiable] = useState<boolean>(false);
  const [dataSearch, setDataSearch] = useState<TypeDataSearch>();
  const [api, contextHolder] = notification.useNotification();

  const openNotification = (value: string) => {
    api.success({
      message: `Notification`,
      description: value,
      placement: "topRight",
    });
  };

  const errorNotification = (value: string) => {
    api.error({
      message: `Notification`,
      description: value,
      placement: "topRight",
    });
  };

  //check event click , show popup create or show popup date or show input search or show delete
  const showModal = (id: number) => {
    if (id === 3) {
      showConfirm();
    } else if (id === 4) {
      setOpenSearch(!openSearch);
    } else {
      setIsModalOpen(true);
    }
  };

  const handleChange = (value: string) => {
    setDataSelected(value);

    form.setFieldsValue({
      group: value,
    });
  };

  const [form] = Form.useForm();
  const [formUpdate] = Form.useForm();

  //Create new records
  const onFinish = (values: any) => {
    const response = { ...values };
    const submit = {
      address: response.address,
      contact1: response.contact1,
      contact2: response.contact2,
      email: response.email,
      group: response.group,
      name: response.name,
      phone1: response.phone1,
      phone2: response.phone2,
      key: dataList.length + 1,
      setting: <SettingOutlined style={{ fontSize: 18, color: "black" }} />,
      age: 32,
    };
    form.resetFields();
    setDataList([...dataList, submit]);
    openNotification("Thêm mới thành công");

    setIsModalOpen(false);
  };

  const { confirm } = Modal;

  //show Confirm delete record
  const showConfirm = () => {
    confirm({
      icon: <ExclamationCircleOutlined />,
      content: <p>Xác nhận xóa 1 nhà cung cấp?</p>,
      onOk() {
        const response = buttonRef?.current?.someExposedProperty();
        console.log("rees", response);
        if (_.isEmpty(response)) {
          errorNotification("Bạn chưa chọn record để xoá");
        } else {
          const formatValue: {
            key: number;
            setting: JSX.Element;
            name: string;
            age: number;
            address: string;
            contact1: string;
            contact2: string;
            email: string;
            group: string;
          }[] = [];
          for (let i = 0; i < dataList.length; i++) {
            response.map((item: any) => {
              if (Number(item) === Number(dataList[i].key)) {
                formatValue.push({
                  ...dataList[i],
                });
              }
            });
          }
          const differentValue = _.differenceWith(
            dataList,
            formatValue,
            _.isEqual
          );
          setDataList(differentValue);
        }
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const onChangeProduct = (e: any) => {
    setNameProduct(e.target.value);
  };
  const onChangeEmail = (e: any) => {
    setEmail(e.target.value);
  };

  //function search Data in form
  const onSearch = () => {
    const response = dataList.filter((item) => {
      if (String(item.name) === nameProduct && String(item.email) === email) {
        return item;
      }
    });
    setDataList(response);
  };

  //reSet defaultValue in Form when Input email and productname search empty
  useEffect(() => {
    if (_.isEmpty(email) && _.isEmpty(nameProduct)) {
      setDataList(dataTable);
    }
  }, [email || nameProduct]);

  //function Update Record in form
  const updateRecord = (valueData: any) => {
    const values = valueData;
    if (dataSearch !== undefined) {
      const response = dataList.findIndex(
        (item) => Number(item.key) === dataSearch?.key
      );
      dataList[response].name = values.name;
      dataList[response].address = values.address;
      dataList[response].age = values.age;
      dataList[response].contact1 = values.contact1;
      dataList[response].contact2 = values.contact2;
      dataList[response].email = values.email;
      dataList[response].group = values.group;
      dataList[response].phone1 = values.phone1;
      dataList[response].phone2 = values.phone2;
      setVisiable(false);
      openNotification("Cập nhật thành công");
      formUpdate.resetFields();
    }
  };

  //set default value for Form when show popup Update
  const initValue = () => {
    if (visiable) {
      formUpdate.setFieldsValue({
        name: dataSearch?.name,
        contact1: dataSearch?.contact1,
        contact2: dataSearch?.contact2,
        email: dataSearch?.email,
        age: dataSearch?.age,
        address: dataSearch?.address,
        group: dataSearch?.group,
        phone1: dataSearch?.phone1,
        phone2: dataSearch?.phone2,
      });
    }
  };

  useEffect(() => {
    initValue();
  }, [visiable]);

  const { Option } = Select;

  return (
    <>
      {contextHolder}

      <div className="containerDashboard">
        <div className="topContent">
          <div className="blockTop">
            <div className="topLeft">
              {buttonAction.map((item) => {
                return (
                  <div
                    className="itemButton"
                    style={{ backgroundColor: item.background }}
                    onClick={() => showModal(Number(item.id))}
                  >
                    {item.icon}
                  </div>
                );
              })}
            </div>
            {openSearch && (
              <div className="topRight">
                <Input
                  style={{ width: 200 }}
                  placeholder="Tên nhà cung cấp"
                  onChange={(text) => onChangeProduct(text)}
                  value={nameProduct}
                />
                <Input
                  className="inputCenter"
                  placeholder="Email"
                  onChange={(text) => onChangeEmail(text)}
                  value={email}
                />
                <Button
                  type="primary"
                  size={"large"}
                  style={{ backgroundColor: "#3DB389" }}
                  onClick={onSearch}
                >
                  Tìm kiếm
                </Button>
              </div>
            )}
          </div>
        </div>
        <div className="bottomContent">
          <TableData
            data={dataList}
            ref={buttonRef}
            setvisiable={setVisiable}
            setDataSearch={setDataSearch}
          />
        </div>
      </div>

      <Modal
        open={isModalOpen}
        className="modalContainer"
        style={{ pointerEvents: "auto" }}
        modalRender={() => {
          return (
            <Form
              {...layout}
              className="formModal"
              labelAlign="left"
              colon={false}
              wrapperCol={{ flex: 1 }}
              onFinish={onFinish}
              validateMessages={validateMessages}
              form={form}
            >
              <div className="fullWidth">
                <div className="topModal">
                  <p className="titleTop">Thêm mới nhà cung cấp</p>
                </div>
                <div className="contentModal">
                  <div className="contentLeftModal">
                    <Form.Item
                      name={["name"]}
                      label="Tên nhà cung cấp"
                      rules={[{ required: true }]}
                      className="inputValue"
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Tên nhà cung cấp"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["contact1"]}
                      label="Tên liên hệ 1"
                      rules={[{ required: true }]}
                      className="inputValue"
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Tên liên hệ 1"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["contact2"]}
                      label="Tên liên hệ 2"
                      rules={[{ required: true }]}
                      className="inputValue"
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Tên liên hệ 2"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["email"]}
                      label="Email"
                      rules={[
                        { required: true },
                        {
                          type: "email",
                          message: "Định dạng Email không hợp lệ",
                        },
                      ]}
                      className="inputValue"
                    >
                      <Input className="heightInput" placeholder={"Email"} />
                    </Form.Item>
                  </div>
                  <div className="contentRightModal">
                    <Form.Item
                      name={["address"]}
                      label="Địa chỉ"
                      rules={[{ required: true }]}
                      className="inputValue"
                    >
                      <Input className="heightInput" placeholder={"Địa chỉ"} />
                    </Form.Item>
                    <Form.Item
                      name={["phone1"]}
                      label="Số điện thoại 1"
                      rules={[{ required: true }]}
                      className="inputValue"
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Số điện thoại 1"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["phone2"]}
                      label="Số điện thoại 2"
                      rules={[{ required: true }]}
                      className="inputValue"
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Số điện thoại 2"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["group"]}
                      label="Nhóm sản phẩm"
                      className="inputValue"
                    >
                      <Select
                        className="selectValue"
                        onChange={handleChange}
                        showArrow={false}
                        open={open}
                        onClick={() => setOpen(!open)}
                        placeholder={"Nhóm sản phẩm"}
                        options={[
                          {
                            label: "Manager",
                            options: [
                              { label: "Jack", value: "jack" },
                              { label: "Lucy", value: "lucy" },
                            ],
                          },
                          {
                            label: "Engineer",
                            options: [{ label: "yiminghe", value: "Yiminghe" }],
                          },
                        ]}
                      />
                      <div
                        className="iconSelect"
                        onClick={() => setOpen(!open)}
                      >
                        <DownOutlined />
                      </div>
                    </Form.Item>
                  </div>
                </div>

                <div className="bottomModal">
                  <Button
                    type="primary"
                    size={"large"}
                    className="buttonCancel"
                    onClick={() => setIsModalOpen(false)}
                  >
                    Đóng
                  </Button>
                  <Button
                    type="primary"
                    size={"large"}
                    className="buttonSubmit"
                    htmlType="submit"
                  >
                    Thêm mới
                  </Button>
                </div>
              </div>
            </Form>
          );
        }}
      />

      <Modal
        open={visiable}
        className="modalContainer"
        style={{ pointerEvents: "auto" }}
        modalRender={() => {
          return (
            <Form
              {...layout}
              className="formModal"
              labelAlign="left"
              colon={false}
              wrapperCol={{ flex: 1 }}
              onFinish={updateRecord}
              form={formUpdate}
            >
              <div className="fullWidth">
                <div className="topModal">
                  <p className="titleTop">Cập nhật nhà cung cấp </p>
                </div>
                <div className="contentModal">
                  <div className="contentLeftModal">
                    <Form.Item
                      name={["name"]}
                      label="Tên nhà cung cấp"
                      className="inputValue"
                      shouldUpdate
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Tên nhà cung cấp"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["contact1"]}
                      label="Tên liên hệ 1"
                      className="inputValue"
                      shouldUpdate
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Tên liên hệ 1"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["contact2"]}
                      label="Tên liên hệ 2"
                      className="inputValue"
                      shouldUpdate
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Tên liên hệ 2"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["email"]}
                      label="Email"
                      className="inputValue"
                      shouldUpdate
                    >
                      <Input className="heightInput" placeholder={"Email"} />
                    </Form.Item>
                  </div>
                  <div className="contentRightModal">
                    <Form.Item
                      name={["address"]}
                      label="Địa chỉ"
                      className="inputValue"
                      shouldUpdate
                    >
                      <Input className="heightInput" placeholder={"Địa chỉ"} />
                    </Form.Item>
                    <Form.Item
                      name={["phone1"]}
                      label="Số điện thoại 1"
                      className="inputValue"
                      shouldUpdate
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Số điện thoại 1"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["phone2"]}
                      label="Số điện thoại 2"
                      className="inputValue"
                      shouldUpdate
                    >
                      <Input
                        className="heightInput"
                        placeholder={"Số điện thoại 2"}
                      />
                    </Form.Item>
                    <Form.Item
                      name={["group"]}
                      label="Nhóm sản phẩm"
                      className="inputValue"
                    >
                      <Select defaultValue={dataSearch?.group}>
                        <Option value="1">Option 1</Option>
                        <Option value="2">Option 2</Option>
                        <Option value="3">Option 3</Option>
                      </Select>
                    </Form.Item>
                  </div>
                </div>

                <div className="bottomModal">
                  <Button
                    type="primary"
                    size={"large"}
                    className="buttonCancel"
                    onClick={() => (
                      setVisiable(false), formUpdate.resetFields()
                    )}
                  >
                    Đóng
                  </Button>
                  <Button
                    type="primary"
                    size={"large"}
                    className="buttonSubmit"
                    htmlType="submit"
                  >
                    Cập nhật
                  </Button>
                </div>
              </div>
            </Form>
          );
        }}
      />
    </>
  );
};

export default DastBoardScreen;
