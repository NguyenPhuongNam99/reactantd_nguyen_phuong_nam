export interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
  contact1: string;
  contact2: string;
  email: string;
  group: string;
  setting: any;
  phone?: string;
  phone2?: string
}


export interface TypeDataSearch {
  address: string,
  age: number,
  contact1: string,
  contact2: string,
  email: string,
  group: string,
  key: number,
  name: string,
  setting: any,
  phone1?: string,
  phone2?: string
}