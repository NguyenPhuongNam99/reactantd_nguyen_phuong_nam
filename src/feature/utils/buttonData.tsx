import {
  CloseCircleOutlined,
  CloseSquareOutlined,
  PlusCircleOutlined,
  SearchOutlined,
  SettingOutlined,
  UnorderedListOutlined,
} from "@ant-design/icons";
import { ColumnsType } from "antd/es/table";
import { DataType } from "./type";

export const buttonAction = [
  {
    id: "1",
    icon: <UnorderedListOutlined style={{ fontSize: 18, color: "white" }} />,
    background: "#00B4FA",
  },
  {
    id: "2",
    icon: <PlusCircleOutlined style={{ fontSize: 18, color: "white" }} />,
    background: "#00B4FA",
  },
  {
    id: "3",
    icon: <CloseSquareOutlined style={{ fontSize: 18, color: "white" }} />,
    background: "#00B4FA",
  },
  {
    id: "4",
    icon: <SearchOutlined  style={{ fontSize: 18, color: "white" }} />,
    background: "#00B4FA",
  },

];

export const columns: ColumnsType<DataType> = [
  {
    title: " # ",
    dataIndex: "setting",
    width: "4%",
  },
  {
    title: "Tên nhà cung cấp",
    dataIndex: "name",
  },
  {
    title: "Địa chỉ",
    dataIndex: "address",
  },
  {
    title: "Liên hệ 1",
    dataIndex: "contact1",
  },
  {
    title: "Liên hệ 2",
    dataIndex: "contact2",
  },
  {
    title: "Email",
    dataIndex: "email",
  },
  {
    title: "Nhóm",
    dataIndex: "group",
  },
];


export const dataTable = [
  {
    key: 1,
    setting: <SettingOutlined style={{ fontSize: 18, color: "black" }} />,
    name: `Edward King1`,
    age: 32,
    address: `London, Park Lane no.`,
    contact1: "Thạch Thất, Hà Nội",
    contact2: "Thạch Thất, Hà Nội",
    email: "nguyenphuongnam06012000@gmail.com",
    group: "TT Viet Nam",
    phone1: '0973712797',
    phone2: '0914255114'
  },
  {
    key: 2,
    setting: <SettingOutlined style={{ fontSize: 18, color: "black" }} />,
    name: `Edward King2`,
    age: 32,
    address: `London, Park Lane no.`,
    contact1: "Thạch Thất, Hà Nội",
    contact2: "Thạch Thất, Hà Nội",
    email: "nguyenphuongnam06012000@gmail.com",
    group: "TT Viet Nam",
    phone1: '0973712797',
    phone2: '0914255114'
  },
  {
    key: 3,
    setting: <SettingOutlined style={{ fontSize: 18, color: "black" }} />,
    name: `Edward King3`,
    age: 32,
    address: `London, Park Lane no.`,
    contact1: "Thạch Thất, Hà Nội",
    contact2: "Thạch Thất, Hà Nội",
    email: "nguyenphuongnam06012000@gmail.com",
    group: "TT Viet Nam",
    phone1: '0973712797',
    phone2: '0914255114'
  },
  {
    key:4,
    setting: <SettingOutlined style={{ fontSize: 18, color: "black" }} />,
    name: `Nam`,
    age: 32,
    address: `Thạch Thất, Hà Nội.`,
    contact1: "Thạch Thất, Hà Nội",
    contact2: "Thạch Thất, Hà Nội",
    email: "nguyenphuongnam06012000@gmail.com",
    group: "TT Viet Nam",
    phone1: '0973712797',
    phone2: '0914255114'
  },
  {
    key:5,
    setting: <SettingOutlined style={{ fontSize: 18, color: "black" }} />,
    name: `Manchester United`,
    age: 32,
    address: `Thạch Thất, Hà Nội.`,
    contact1: "Thạch Thất, Hà Nội",
    contact2: "Thạch Thất, Hà Nội",
    email: "nguyenphuongnam06012000@gmail.com",
    group: "TT Viet Nam",
    phone1: '0973712797',
    phone2: '0914255114'
  },
  {
    key:6,
    setting: <SettingOutlined style={{ fontSize: 18, color: "black" }} />,
    name: `NguyenPhuongNam`,
    age: 32,
    address: `Thạch Thất, Hà Nội.`,
    contact1: "Thạch Thất, Hà Nội",
    contact2: "Thạch Thất, Hà Nội",
    email: "nguyenphuongnam06012000@gmail.com",
    group: "TT Viet Nam",
    phone1: '0973712797',
    phone2: '0914255114'
  }
]
