import React, { useState } from "react";
import "./index.scss";
import {
  AppstoreOutlined,
  DownOutlined,
  MailOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from "@ant-design/icons";
import { Layout, Menu, MenuProps, theme } from "antd";
import { Outlet, useNavigate } from "react-router-dom";

const { Header, Sider, Content } = Layout;
type MenuItem = Required<MenuProps>["items"][number];

const Home: React.FC = () => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();

  const {
    token: { colorBgContainer },
  } = theme.useToken();

  function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: "group"
  ): MenuItem {
    return {
      key,
      icon,
      children,
      label,
      type,
    } as MenuItem;
  }

  const items: MenuProps["items"] = [
    getItem("Quản lý người dùng", "sub1", <MailOutlined />, [
      getItem("Option 1", "1"),
      getItem("Option 2", "2"),
    ]),

    getItem("Danh mục", "sub2", <AppstoreOutlined />, [
      getItem("Nhà cung cấp", "3"),
      getItem("Option 6", "4"),
    ]),
    { type: "divider" },
  ];
  const onClick = (data: any) => {
    console.log("onclick", data);
  };

  return (
    <Layout className="homeContainer">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo">
          <img src={require("./logo.png")} />
          {!collapsed && <p className="titleHeader">Quản lý sản xuất</p>}
        </div>
        <Menu
          onClick={onClick}
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["1"]}
          items={items}
          onSelect={(selectkey) => {
            console.log("seleted", selectkey);
            if (Number(selectkey.key) === Number(1)) {
              console.log("v");
              navigate("/");
            }
            if (Number(selectkey.key) === Number(2)) {
              navigate("/Option2");
            }
            if (Number(selectkey.key) === Number(3)) {
              navigate("/");
            }
            if (Number(selectkey.key) === Number(4)) {
              navigate("/Option4");
            }
          }}
        ></Menu>
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            background: colorBgContainer,
            flexDirection: "row",
            display: "flex",
            paddingLeft: 17,
          }}
        >
          <div className="headerLeft">
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: () => setCollapsed(!collapsed),
              }
            )}

            <p style={{ color: "#4B7BF8" }}>Danh mục</p>
            <p>/ Nhà cung cấp</p>
          </div>
          <div className="headerRight">
            <p style={{ fontStyle: "italic", fontSize: 16, color: "#000000" }}>
              Nguyễn Phương Nam
            </p>
            <div className="blockAvatar">N</div>
            <DownOutlined />
          </div>
        </Header>
        <Content
          style={{
            margin: "18px 16px",
            padding: 18,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default Home;
