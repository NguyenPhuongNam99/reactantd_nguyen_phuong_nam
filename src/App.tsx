import React from "react";
import "./App.scss";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./feature/home/Home";
import EmptyPage from "./feature/emptyPage/emptyPage";
import DastBoardScreen from "./feature/Dashboard/Dashboard";

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Home />,
      children: [
        {
          path: "/",
          element: <DastBoardScreen />,
        },
        {
          path: "/Option2",
          element: <EmptyPage />,
        },
         {
          path: "/Option3",
          element: <DastBoardScreen />,
        },
         {
          path: "/Option4",
          element: <EmptyPage />,
        },
      ],
    },
    {
      path: "/Option2",
      element: <EmptyPage />,
    },
  ]);
  return <RouterProvider router={router} />;
}

export default App;
